package cz.fit.miadp.mvcgame.controller;

import cz.fit.miadp.mvcgame.command.*;
import cz.fit.miadp.mvcgame.model.GameModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.awt.event.KeyEvent;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GameControllerTest {

    @Mock
    private GameModel gameModel;

    private GameController gameController;

    @Before
    public void prepareGameController() {
        gameController = new GameController();
        gameController.setModel(gameModel);
    }

    @Test
    public void testVK_UP() {
        testCommon(KeyEvent.VK_UP, MoveCannonUpCommand.class);
    }

    @Test
    public void testVK_DOWN() {
        testCommon(KeyEvent.VK_DOWN, MoveCannonDownCommand.class);
    }

    @Test
    public void testVK_SPACE() {
        testCommon(KeyEvent.VK_SPACE, CannonShootCommand.class);
    }

    @Test
    public void testVK_A() {
        testCommon(KeyEvent.VK_A, CannonAimUpCommand.class);
    }

    @Test
    public void testVK_S() {
        testCommon(KeyEvent.VK_S, IncCannonPowerCommand.class);
    }

    @Test
    public void testVK_X() {
        testCommon(KeyEvent.VK_X, DecCannonPowerCommand.class);
    }

    @Test
    public void testVK_Q() {
        testCommon(KeyEvent.VK_Q, SwitchMovementStrategyCommand.class);
    }

    @Test
    public void testVK_W() {
        testCommon(KeyEvent.VK_W, SwitchShootingModeCommand.class);
    }

    /**
     * Tests correct behaviour in case of KeyEvent is not recognized and not mapped to any action
     * Correct behaviour = {@link GameModel#registerCmd(AbsGameCommand)} method is not invoked
     */
    @Test
    public void testNoAction() {
        KeyEvent keyEvent = Mockito.mock(KeyEvent.class);
        when(keyEvent.getKeyCode()).thenReturn(KeyEvent.VK_T);

//        action
        gameController.onKeyPress(keyEvent);

//        verification
        verify(gameModel, never()).registerCmd(any());

//        second way to verify
        verifyZeroInteractions(gameModel);
    }

    @Test
    public void testVK_Z_CTRL_MASK() {
        KeyEvent keyEvent = Mockito.mock(KeyEvent.class);
        when(keyEvent.getKeyCode()).thenReturn(KeyEvent.VK_Z);
        when(keyEvent.getModifiers()).thenReturn(KeyEvent.CTRL_MASK);

//        action
        gameController.onKeyPress(keyEvent);

//        verification
        verify(gameModel).registerCmd(isA(UndoLastCommand.class));
    }

    @Test
    public void testVK_Z() {
        KeyEvent keyEvent = Mockito.mock(KeyEvent.class);
        when(keyEvent.getKeyCode()).thenReturn(KeyEvent.VK_Z);
//        mask different to CTRL_MASK
        when(keyEvent.getModifiers()).thenReturn(KeyEvent.SHIFT_MASK);

//        action
        gameController.onKeyPress(keyEvent);

//        verification
        verify(gameModel).registerCmd(isA(CannonAimDownCommand.class));
    }

    /**
     * Common part of all tests in {@link GameControllerTest} class
     *
     * @param keyEventInteger identification of key event to pass to {@link GameController#onKeyPress(KeyEvent)}
     * @param clazz           parameter of {@link GameModel#registerCmd(AbsGameCommand)} that has to be invoked with
     */
    private void testCommon(Integer keyEventInteger, Class<? extends AbsGameCommand> clazz) {
        KeyEvent keyEvent = Mockito.mock(KeyEvent.class);
        when(keyEvent.getKeyCode()).thenReturn(keyEventInteger);

//        action
        gameController.onKeyPress(keyEvent);

//        verification
        verify(gameModel).registerCmd(isA(clazz));

//        second way to verify
        ArgumentCaptor<AbsGameCommand> argument = ArgumentCaptor.forClass(AbsGameCommand.class);
        verify(gameModel).registerCmd(argument.capture());
        assertEquals(clazz.getSimpleName(), argument.getValue().getClass().getSimpleName());
    }

}