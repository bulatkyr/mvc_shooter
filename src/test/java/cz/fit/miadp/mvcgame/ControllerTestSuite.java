package cz.fit.miadp.mvcgame;

import cz.fit.miadp.mvcgame.controller.GameControllerTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        GameControllerTest.class
//        here goes other test that are in the same package or test related functionality, so suits make any sense
})
public class ControllerTestSuite {
}
