package cz.fit.miadp.mvcgame;


import cz.fit.miadp.mvcgame.model.CannonTest;
import cz.fit.miadp.mvcgame.model.GameModelTest;
import cz.fit.miadp.mvcgame.model.GameObjectTest;
import cz.fit.miadp.mvcgame.model.ModelInfoTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        CannonTest.class,
        GameModelTest.class,
        GameObjectTest.class,
        ModelInfoTest.class
})
public class ModelTestSuite {
}
