package cz.fit.miadp.mvcgame.model;

import cz.fit.miadp.mvcgame.state.DoubleShootingMode;
import cz.fit.miadp.mvcgame.strategy.BounceMovementStrategy;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ModelInfoTest {

    @Test
    public void testGetText() {
        GameModel model = mock(GameModel.class);

        Cannon cannon = Mockito.mock(Cannon.class);
        when(cannon.getY()).thenReturn(80);
        when(cannon.getAngle()).thenReturn(-0.1f);
        when(cannon.getPower()).thenReturn(17f);
        when(cannon.getShootingMode()).thenReturn(new DoubleShootingMode());

        ArrayList<Missile> missiles = Mockito.mock(ArrayList.class);
        when(missiles.size()).thenReturn(4);

        BounceMovementStrategy movementStrategy = new BounceMovementStrategy(100, 100);

        when(model.getScore()).thenReturn(5);
        when(model.getCannon()).thenReturn(cannon);
        when(model.getMissiles()).thenReturn(missiles);
        when(model.getActiveMovementStrategy()).thenReturn(movementStrategy);

//        action
        ModelInfo modelInfo = new ModelInfo(model);
        String expected = "Score: 5 Cannon.Y: 80 Cannon.Angle: -0.1 Cannon.Power: 17.0 " +
                "Missile.size: 4 MoveStrg: Bounce ShootMode: Double";

//        verification
        assertEquals(expected, modelInfo.getText());
    }

}