package cz.fit.miadp.mvcgame.model;

import cz.fit.miadp.mvcgame.abstractFactory.DefaultGameObjectsFactory;
import cz.fit.miadp.mvcgame.config.GameConfig;
import cz.fit.miadp.mvcgame.state.DoubleShootingMode;
import cz.fit.miadp.mvcgame.state.SingleShootingMode;
import cz.fit.miadp.mvcgame.strategy.BounceMovementStrategy;
import cz.fit.miadp.mvcgame.strategy.RandomMovementStrategy;
import cz.fit.miadp.mvcgame.visitor.IVisitor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class CannonTest {


    private static final double DELTA = 1E-6;

    private DefaultGameObjectsFactory factory;

    @Before
    public void beforeTest() {
        factory = Mockito.mock(DefaultGameObjectsFactory.class);
    }

    @Test
    public void testConstructorShootingMode() {
        Cannon cannon = new Cannon(factory);

        assertNotNull(cannon.getShootingMode());
        assertEquals(new SingleShootingMode().getName(), cannon.getShootingMode().getName());
    }

    @Test
    public void testSingleShootingMode() {
        Missile missile = new Missile(1, 1, 1.6f, 1.5f, new BounceMovementStrategy(500, 500));
        Mockito.when(factory.createMissile()).thenReturn(missile);

        Cannon cannon = new Cannon(factory);
        List<Missile> shootBatch = cannon.shoot();
        cannon.toggleShootingMode();

        assertNotNull(shootBatch);
        assertEquals(1, shootBatch.size());
        assertEquals(missile, shootBatch.get(0));
//        verification of new shooting mode
        assertEquals(new DoubleShootingMode().getName(), cannon.getShootingMode().getName());
    }

    @Test
    public void testDoubleShootingMode() {
        Missile missile = new Missile(1, 1, 1.6f, 1.5f, new BounceMovementStrategy(500, 500));
        Missile missile1 = new Missile(2, 2, 2.6f, 2.5f, new RandomMovementStrategy());
        Mockito.when(factory.createMissile()).thenReturn(missile).thenReturn(missile1);

//        action
        Cannon cannon = new Cannon(factory);
        cannon.setDoubleShootingMode();
        List<Missile> shootBatch = cannon.shoot();
        cannon.toggleShootingMode();

//        verification
        assertNotNull(shootBatch);
        assertEquals(2, shootBatch.size());
        assertEquals(missile, shootBatch.get(0));
        assertEquals(missile1, shootBatch.get(1));
//        verification the angle is not changed
        assertEquals(0, cannon.getAngle(), DELTA);
//        verification of new shooting mode
        assertEquals(new SingleShootingMode().getName(), cannon.getShootingMode().getName());
    }

    @Test
    public void testAimUp() {
//        action
        Cannon cannon = new Cannon(factory);
        cannon.aimUp();

//        verification
        assertEquals(-GameConfig.ANGLE_STEP, cannon.getAngle(), DELTA);
    }

    @Test
    public void testAimDown() {
//        action
        Cannon cannon = new Cannon(factory);
        cannon.aimDown();

//        verification
        assertEquals(GameConfig.ANGLE_STEP, cannon.getAngle(), DELTA);
    }

    @Test
    public void testIncPower() {
//        action
        Cannon cannon = new Cannon(factory);
        cannon.incPower();

//        verification
        assertEquals(10.0f + GameConfig.POWER_STEP, cannon.getPower(), DELTA);
    }

    @Test
    public void testDecPower() {
//        action
        Cannon cannon = new Cannon(factory);
        cannon.decPower();

//        verification
        assertEquals(10.0f - GameConfig.POWER_STEP, cannon.getPower(), DELTA);
    }

    @Test
    public void testVisitor() {
        IVisitor visitor = Mockito.mock(IVisitor.class);

        Cannon cannon = new Cannon(factory);
        cannon.acceptVisitor(visitor);

        Cannon cannon1 = new Cannon(factory);

        Mockito.verify(visitor).visitCannon(cannon);
        Mockito.verify(visitor, Mockito.never()).visitCannon(cannon1);
    }

}