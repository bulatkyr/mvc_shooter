package cz.fit.miadp.mvcgame.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GameObjectTest {

    @Test
    public void testCollidesWith() {
        testCollisionCommon(101, 101, true);
    }

    @Test
    public void testCollidesWithNegativeCase() {
        testCollisionCommon(150, 130, false);
    }

    private void testCollisionCommon(int x, int y, boolean expectedResult) {
        GameObject gameObject = Mockito.mock(GameObject.class);
        when(gameObject.getX()).thenReturn(100);
        when(gameObject.getY()).thenReturn(100);
        Mockito.doCallRealMethod().when(gameObject).collidesWith(Matchers.anyObject());

        Enemy enemy = new Enemy();
        enemy.setX(x);
        enemy.setY(y);

        boolean result = gameObject.collidesWith(enemy);
        assertEquals(expectedResult, result);
    }

}