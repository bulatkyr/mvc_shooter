package cz.fit.miadp.mvcgame.model;

import cz.fit.miadp.mvcgame.command.AbsGameCommand;
import cz.fit.miadp.mvcgame.config.GameConfig;
import cz.fit.miadp.mvcgame.strategy.BounceMovementStrategy;
import cz.fit.miadp.mvcgame.strategy.IMovementStrategy;
import cz.fit.miadp.mvcgame.strategy.RandomMovementStrategy;
import cz.fit.miadp.mvcgame.strategy.SimpleMovementStrategy;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class GameModelTest {


    @Test
    public void testConstructor() {
        GameModel gameModel = new GameModel();

        assertNotNull(gameModel.getGoFact());
        List<IMovementStrategy> movementStrategies = gameModel.getMovementStrategies();
        assertNotNull(movementStrategies);
        assertEquals(3, movementStrategies.size());
        assertEquals(0, gameModel.getScore());
        assertNotNull(gameModel.getCannon());
        assertNotNull(gameModel.getInfo());
        ArrayList<Enemy> enemies = gameModel.getEnemies();
        assertNotNull(enemies);
        assertEquals(GameConfig.MAX_ENEMIES, enemies.size());
    }

    /**
     * Tests usual test case of undo last command
     */
    @Test
    public void testUndoLastCommand() {
        GameModel gameModel = new GameModel();

        AbsGameCommand command = Mockito.mock(AbsGameCommand.class);
        AbsGameCommand command1 = Mockito.mock(AbsGameCommand.class);
        Stack<AbsGameCommand> commands = new Stack<>();
        commands.push(command);
        commands.push(command1);
        Whitebox.setInternalState(gameModel, "executedCmds", commands);

        gameModel.undoLastCommand();

        Mockito.verify(command1, Mockito.never()).unexecute();
        Mockito.verify(command1, Mockito.never()).execute();

        Mockito.verify(command).unexecute();

        assertEquals(0, gameModel.getExecutedCmds().size());
    }

    /**
     * Test that no commands are popped out from stack if stack is empty
     */
    @Test
    public void testUndoLatCommandEmpty() {
        GameModel gameModel = new GameModel();

        Stack commands = Mockito.mock(Stack.class);
        Mockito.when(commands.empty()).thenReturn(true);
        Whitebox.setInternalState(gameModel, "executedCmds", commands);

        gameModel.undoLastCommand();

        Mockito.verify(commands, Mockito.never()).pop();
    }

    /**
     * Test that on the start of the game there is no command to undo
     */
    @Test
    public void testUndoLatCommandStart() {
        GameModel gameModel = new GameModel();

        Stack commands = Mockito.mock(Stack.class);
        Mockito.when(commands.pop()).thenReturn(null);
        Mockito.when(commands.empty()).thenReturn(false).thenReturn(true);
        Whitebox.setInternalState(gameModel, "executedCmds", commands);

        gameModel.undoLastCommand();

        Mockito.verify(commands, Mockito.times(1)).pop();
    }

    @Test
    public void testSwitchMovementStrategy() {
        GameModel gameModel = new GameModel();

        assertEquals(new SimpleMovementStrategy().getName(), gameModel.getActiveMovementStrategy().getName());
        gameModel.switchMovementStrategy();
        assertEquals(new RandomMovementStrategy().getName(), gameModel.getActiveMovementStrategy().getName());
        gameModel.switchMovementStrategy();
        assertEquals(new BounceMovementStrategy(gameModel.getMaxX(), gameModel.getMaxY()).getName(),
                gameModel.getActiveMovementStrategy().getName());
        gameModel.switchMovementStrategy();
        assertEquals(new SimpleMovementStrategy().getName(), gameModel.getActiveMovementStrategy().getName());
    }

    @Test
    public void testMoveGameObjects() {
        GameModel gameModel = Mockito.mock(GameModel.class);
        doCallRealMethod().when(gameModel).moveGameObjects();
//        doCallRealMethod().when(gameModel).notifyMyObservers();
//        doNothing().when(gameModel).initTimer();

        gameModel.moveGameObjects();

//        verification of invoked methods
        Mockito.verify(gameModel, times(1)).moveGameObjects();
        Mockito.verify(gameModel, times(1)).moveMissiles();
        Mockito.verify(gameModel, times(1)).destroyInvisibleObjects();
        Mockito.verify(gameModel, times(1)).handleCollisions();
        Mockito.verify(gameModel, times(1)).notifyMyObservers();
    }
}