package cz.fit.miadp.mvcgame.strategy;

public class BounceMovementStrategy implements IMovementStrategy {

    private static final int MIN_DISTANCE = 2;

    private int maxX;
    private int maxY;

    public BounceMovementStrategy(int maxX, int maxY) {
        this.maxX = maxX;
        this.maxY = maxY;
    }

    @Override
    public String getName() {
        return "Bounce";
    }

    @Override
    public int nextPosX(int initX, float initPower, float initAngle, long lifetime) {
        int newX = (int) (initX + (lifetime * initPower * Math.cos(initAngle)));
        if (newX + MIN_DISTANCE > maxX) {
//            newX = max - (newX + MIN_DISTANCE - maxX);
            newX = 2 * maxX - newX - MIN_DISTANCE;
        }
        return newX;
    }

    @Override
    public int nextPosY(int initY, float initPower, float initAngle, long lifetime) {
        int newY = (int) (initY + (lifetime * initPower * Math.sin(initAngle)));
        if (newY - MIN_DISTANCE < 0) {
            newY = 0 - (newY - MIN_DISTANCE);
        }
        if (newY + MIN_DISTANCE > maxY) {
            newY = maxY - (newY + MIN_DISTANCE - maxY);
        }
        return newY;
    }
}
