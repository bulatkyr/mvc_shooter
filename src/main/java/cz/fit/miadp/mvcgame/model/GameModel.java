package cz.fit.miadp.mvcgame.model;

import cz.fit.miadp.mvcgame.abstractFactory.DefaultGameObjectsFactory;
import cz.fit.miadp.mvcgame.abstractFactory.IGameObjectsFactory;
import cz.fit.miadp.mvcgame.command.AbsGameCommand;
import cz.fit.miadp.mvcgame.config.GameConfig;
import cz.fit.miadp.mvcgame.observer.IObservable;
import cz.fit.miadp.mvcgame.observer.IObserver;
import cz.fit.miadp.mvcgame.proxy.IGameModel;
import cz.fit.miadp.mvcgame.strategy.BounceMovementStrategy;
import cz.fit.miadp.mvcgame.strategy.IMovementStrategy;
import cz.fit.miadp.mvcgame.strategy.RandomMovementStrategy;
import cz.fit.miadp.mvcgame.strategy.SimpleMovementStrategy;

import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

public class GameModel implements IObservable, IGameModel {
    private Cannon cannon;
    private ModelInfo info;
    private ArrayList<Enemy> enemies = new ArrayList<>();
    private ArrayList<Missile> missiles = new ArrayList<>();
    private ArrayList<Collision> collisions = new ArrayList<>();

    private int score = GameConfig.INIT_SCORE;

    private List<IObserver> myObservers = new ArrayList<IObserver>();
    private Queue<AbsGameCommand> unexecutedCmds = new LinkedBlockingQueue<AbsGameCommand>();
    private Stack<AbsGameCommand> executedCmds = new Stack<AbsGameCommand>();

    private IGameObjectsFactory goFact;
    private List<IMovementStrategy> movementStrategies = new ArrayList<IMovementStrategy>();
    private int activeMovementStrategyIndex = 0;

    public GameModel() {
        this.goFact = new DefaultGameObjectsFactory(this);

        this.movementStrategies.add(new SimpleMovementStrategy());
        this.movementStrategies.add(new RandomMovementStrategy());
        this.movementStrategies.add(new BounceMovementStrategy(this.getMaxX(), this.getMaxY()));

        initGame();
        initTimer();
    }

    public ArrayList<Collision> getCollisions() {
        return collisions;
    }

    protected void initTimer() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                // THE GAME LOOP
                executeCmds();
                moveGameObjects();
            }
        }, 0, GameConfig.TIME_TICK);
    }

    private void executeCmds() {
        while (!this.unexecutedCmds.isEmpty()) {
            AbsGameCommand cmd = this.unexecutedCmds.poll();
            this.executedCmds.push(cmd);
            // extExecute instead of execute
            cmd.extExecute();
        }
    }

    public void registerCmd(AbsGameCommand cmd) {
        this.unexecutedCmds.add(cmd);
    }

    public void undoLastCommand() {
        if (this.executedCmds != null && !this.executedCmds.empty()) {
            this.executedCmds.pop(); // remove UndoLastCommand
            // pop a Command executed just before UndoLastCommand
            if (!this.executedCmds.empty()) {
                AbsGameCommand cmd = this.executedCmds.pop();
                cmd.unexecute();
            }
        }
    }

    void moveGameObjects() {
        moveMissiles();
        destroyInvisibleObjects();

        handleCollisions();

        this.notifyMyObservers();
    }

    void handleCollisions() {
        List<Missile> missToRemove = new ArrayList<>();
        List<Enemy> enemsToRemove = new ArrayList<>();

        for (Missile m : this.getMissiles()) {
            for (Enemy e : this.getEnemies()) {
                if (m.collidesWith(e)) {
                    // remove m & e
                    missToRemove.add(m);
                    enemsToRemove.add(e);

                    // inc score
                    this.score++;

                    // create Collision
                    collisions.add(goFact.createCollision(m.getX(), m.getY()));
                }
            }
        }

        // collect old collisions
        List<Collision> collsToRemove = new ArrayList<Collision>();
        for (Collision c : this.collisions) {
            if (c.getLifetime() > GameConfig.COLLISION_LIFETIME) {
                collsToRemove.add(c);
            }
        }

        // remove stuff
        for (Collision c : collsToRemove)
            this.collisions.remove(c);
        for (Enemy e : enemsToRemove)
            this.enemies.remove(e);
        for (Missile m : missToRemove)
            this.missiles.remove(m);

    }

    void destroyInvisibleObjects() {
        Set<Missile> toRemove = new HashSet<>();
        for (Missile m : this.getMissiles()) {
            if (m.getX() < 0 || m.getX() > this.getMaxX())
                toRemove.add(m);

            if (m.getY() < 0 || m.getY() > this.getMaxY())
                toRemove.add(m);
        }

        for (Missile m : toRemove)
            this.missiles.remove(m);
    }

    void moveMissiles() {
        for (Missile missile : this.getMissiles())
            missile.move();
    }

    private void initGame() {
        this.score = 0;
        this.cannon = this.goFact.createCannon();
        this.info = this.goFact.createModelInfo();

        this.enemies.clear();
        for (int i = 0; i < GameConfig.MAX_ENEMIES; i++)
            this.enemies.add(this.goFact.createEnemy());
    }

    public int getScore() {
        return this.score;
    }

    public int getMaxX() {
        return GameConfig.MAX_X;
    }

    public int getMaxY() {
        return GameConfig.MAX_Y;
    }

    public ArrayList<Enemy> getEnemies() {
        return this.enemies;
    }

    public Cannon getCannon() {
        return this.cannon;
    }

    public ModelInfo getInfo() {
        return this.info;
    }

    public void moveCannonUp() {
        int y = this.cannon.getY();
        y -= GameConfig.MOVE_STEP;
        this.cannon.setY(y);

        this.notifyMyObservers();
    }

    public void moveCannonDown() {
        int y = this.cannon.getY();
        y += GameConfig.MOVE_STEP;
        this.cannon.setY(y);

        this.notifyMyObservers();
    }

    public void switchMovementStrategy() {
        activeMovementStrategyIndex = (activeMovementStrategyIndex + 1) % movementStrategies.size();
        this.notifyMyObservers();
    }

    public void attachObserver(IObserver observer) {
        if (!this.myObservers.contains(observer))
            this.myObservers.add(observer);
    }

    public void deattachObserver(IObserver observer) {
        this.myObservers.remove(observer);
    }

    public void notifyMyObservers() {
        for (IObserver obs : this.myObservers) {
            obs.update();
        }
    }

    public ArrayList<Missile> getMissiles() {
        return this.missiles;
    }

    public void cannonShoot() {
        this.missiles.addAll(this.cannon.shoot());

        this.notifyMyObservers();
    }

    public void cannonToggleShootingMode() {
        this.cannon.toggleShootingMode();
    }

    public void aimCannonUp() {
        this.cannon.aimUp();

        this.notifyMyObservers();
    }

    public void aimCannonDown() {
        this.cannon.aimDown();

        this.notifyMyObservers();
    }

    public void incCannonPower() {
        this.cannon.incPower();

        this.notifyMyObservers();
    }

    public void decCannonPower() {
        this.cannon.decPower();

        this.notifyMyObservers();
    }

    public ArrayList<GameObject> getGameObjects() {
        ArrayList<GameObject> gos = new ArrayList<GameObject>();

        gos.addAll(this.enemies);
        gos.addAll(this.missiles);
        gos.addAll(this.collisions);
        gos.add(this.cannon);
        gos.add(this.getInfo());

        return gos;
    }

    public IMovementStrategy getActiveMovementStrategy() {
        return this.movementStrategies.get(this.activeMovementStrategyIndex);
    }

    public Queue<AbsGameCommand> getUnexecutedCmds() {
        return unexecutedCmds;
    }

    public IGameObjectsFactory getGoFact() {
        return goFact;
    }

    public List<IMovementStrategy> getMovementStrategies() {
        return movementStrategies;
    }

    public Stack<AbsGameCommand> getExecutedCmds() {
        return executedCmds;
    }

    public Object createMemento() {
        Memento memento = new Memento();
        memento.score = this.score;
        memento.activeMovementStrategyIndex = this.activeMovementStrategyIndex;
        memento.enemies.clear();
        try {
            for (Enemy enemy : this.getEnemies()) {
                memento.enemies.add((Enemy) enemy.clone());
            }
            memento.cannon = (Cannon) this.cannon.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return memento;
    }

    private class Memento {
        int score;
        int activeMovementStrategyIndex;
        Cannon cannon;
        ArrayList<Enemy> enemies = new ArrayList<>();
    }

    public void setMemento(Object memento) {
        Memento m = (Memento) memento;
        this.score = m.score;
        this.activeMovementStrategyIndex = m.activeMovementStrategyIndex;
        this.enemies = m.enemies;
        this.cannon = m.cannon;
    }
}