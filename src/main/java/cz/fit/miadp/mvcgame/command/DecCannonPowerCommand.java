package cz.fit.miadp.mvcgame.command;

import cz.fit.miadp.mvcgame.proxy.IGameModel;

public class DecCannonPowerCommand extends AbsGameCommand {
    public DecCannonPowerCommand(IGameModel subject) {
        super(subject);
    }

    @Override
    public void execute() {
        this.subject.decCannonPower();
    }
}
