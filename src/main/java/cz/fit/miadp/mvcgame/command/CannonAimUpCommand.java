package cz.fit.miadp.mvcgame.command;

import cz.fit.miadp.mvcgame.proxy.IGameModel;

public class CannonAimUpCommand extends AbsGameCommand {

    public CannonAimUpCommand(IGameModel subject) {
        super(subject);
    }

    @Override
    public void execute() {
        this.subject.aimCannonUp();
    }
}
