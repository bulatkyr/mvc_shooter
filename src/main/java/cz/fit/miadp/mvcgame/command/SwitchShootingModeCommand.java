package cz.fit.miadp.mvcgame.command;

import cz.fit.miadp.mvcgame.proxy.IGameModel;

public class SwitchShootingModeCommand extends AbsGameCommand {
    public SwitchShootingModeCommand(IGameModel subject) {
        super(subject);
    }

    @Override
    public void execute() {
        this.subject.cannonToggleShootingMode();
    }
}
