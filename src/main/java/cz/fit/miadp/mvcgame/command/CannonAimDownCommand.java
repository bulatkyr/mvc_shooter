package cz.fit.miadp.mvcgame.command;

import cz.fit.miadp.mvcgame.proxy.IGameModel;

public class CannonAimDownCommand extends AbsGameCommand {

    public CannonAimDownCommand(IGameModel subject) {
        super(subject);
    }

    @Override
    public void execute() {
        this.subject.aimCannonDown();
    }
}
